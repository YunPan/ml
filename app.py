from flask import Flask, request, jsonify
# from flask_sqlalchemy import SQLAlchemy
# from flask_marshmallow import Marshmallow
from flask_api import status
import time
import re

from algos.time_series import train as tsTrain
from algos.time_series import pred as tsPred
from algos.linreg import train as lrTrain
from algos.linreg import pred as lrPred

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    return "Hello from flask server."


@app.route('/predict', methods=['POST'])
def predict():
    '''given model-path, data, return prediction'''
    try:
        body = request.get_json()
        x = body.get("x")
        algo = body.get("algo") # algo name
        model_path = body.get("model-path") # full path

        # request validation
        if not model_path:
            return jsonify({"error": "Either model-name or model-path is required."}), status.HTTP_400_BAD_REQUEST
        if not x:
            return jsonify({"error": "x is required"}), status.HTTP_400_BAD_REQUEST
        if not algo:
            return jsonify({"error": "algo is required"}), status.HTTP_400_BAD_REQUEST

        # choose algo to predict
        if algo == "time-series":
            y_pred = tsPred.predict_with(x, model_path)
            return jsonify({"y": y_pred.tolist(), "y-shape":y_pred.shape})

        if algo == "linear-regression":
            y_pred = lrPred.predict_with(x, model_path)
            return jsonify({"y": y_pred.tolist(), "y-shape":y_pred.shape})

        # algo not found, error
        return jsonify({"error": f"algo '{algo}' is not supported"}), status.HTTP_400_BAD_REQUEST

    except Exception as e:
        print(e)
        return jsonify({"error": str(e)}), status.HTTP_400_BAD_REQUEST


@app.route('/train', methods=['POST'])
def train():
    '''given desired model name, data, return model name'''
    try:
        body = request.get_json()
        x = body.get("x") # train x
        y = body.get("y") # train y
        algo = body.get("algo") # algo name
        name = body.get('name')

        # request validation
        if not name:
            return jsonify({"error": "name is required"}), status.HTTP_400_BAD_REQUEST
        if not x:
            return jsonify({"error": "x is required"}), status.HTTP_400_BAD_REQUEST
        if not y:
            return jsonify({"error": "y is required"}), status.HTTP_400_BAD_REQUEST
        if not algo:
            return jsonify({"error": "algo is required"}), status.HTTP_400_BAD_REQUEST

        # find algo to train
        if algo == "time-series":
            model_path = tsTrain.train_with(x, y, make_model_path(name))
            return jsonify({"model-path": model_path})

        if algo == "linear-regression":
            model_path = lrTrain.train_with(x, y, make_model_path(name))
            return jsonify({"model-path": model_path})

        # algo not found, error
        return jsonify({"error": f"algo '{algo}' is not supported"}), status.HTTP_400_BAD_REQUEST

    except Exception as e:
        print(e)
        return jsonify({"error": str(e)}), status.HTTP_400_BAD_REQUEST

def make_model_path(model_name):
    '''given model name, returns model path. Not creating directory'''
    subfix = int(time.time())
    return f'models/{clean_name(model_name)}-{subfix}.md'

def clean_name(name):
    return re.sub(r'[^a-zA-Z0-9\.\-\_]+', '', name) # my-time-series.1.1

if __name__ == "__main__":
    app.run(debug=True)
