import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import FeatureUnion

from skits.pipeline import ForecasterPipeline
from skits.preprocessing import ReversibleImputer
from skits.feature_extraction import (AutoregressiveTransformer,
                                      SeasonalTransformer)

import matplotlib.pyplot as plt
import pickle

def train_with(X_list, y_list, model_path):
    steps = [
        ('pre_scaling', StandardScaler()),
        ('features', FeatureUnion([
            ('ar_transformer', AutoregressiveTransformer(num_lags=3)),
            ('seasonal_transformer', SeasonalTransformer(seasonal_period=20)
             )])),
        ('post_features_imputer', ReversibleImputer()),
        ('regressor', LinearRegression(fit_intercept=False))
    ]

    pipeline = ForecasterPipeline(steps)

    X = np.array(X_list)
    y = np.array(y_list)
    pipeline.fit(X, y) # stats?

    pickle.dump(pipeline, open(model_path, 'wb'))
    return model_path


def test2():
    trainReq = {
        "algo": "time-series"
        , "name": "my-time-series"
        , "y": [0.7523793648417161, 2.529169757643311, 0.37723338368658244,
                4.265220701700198, 3.575687412565057, 5.205946488733838,
                4.647590921236449, 3.181209487847961, 3.1342973543078596,
                1.5872499303592211, 1.1632526587026335, -1.3321531826320436,
                -2.7876256096983716, -4.104715473706946, -5.199326929508319,
                -5.7104822580345465, -4.917193464796935, -4.170180717896176,
                -2.912434152473489, -0.7649255740698633, -1.8181844383298196,
                0.6138272928173297, 2.8198967655379166, 3.657444773611683,
                4.065355997081614, 5.340924461579886, 4.660801519267923,
                3.4917981866106818, 1.719671986267815, -0.9965905962165857,
                -1.5159868221053883, 0.023053628461934483, -1.3942704111052848,
                -3.7762377237404783, -4.712577914494647, -5.989601371326241,
                -5.190648524592266, -3.0014943363836215, -4.350029511228147,
                -1.975201263344441, -1.2125450967315816, -0.7324303001695451,
                3.505381340106536, 2.963476457684033, 6.058243701499818,
                4.62158040705248, 5.0074845863700785, 5.153753700276936,
                1.3733079247350108, 2.9452094810896945, -0.5004282319242168,
                -0.591585093138894, -3.269210789754931, -3.5413564503294257,
                -3.093031166321828, -4.547167914731143, -3.9888734399360306,
                -4.735873413308104, -2.312474684121589, -2.493304707377084,
                -0.02879983695809149, 1.0177348074707853, 2.4349406225018537,
                4.553489610681803, 5.634423056022638, 4.39066706914744,
                3.2643862705101414, 4.999179859519946, 4.296450892852596,
                2.823775805680633, 0.5617802054570316, -2.238829683663008,
                -2.6550645541375952, -3.614580227294506, -2.8550150314822487,
                -5.956771534325955, -6.72976376889799, -3.5324394386286486,
                -1.839721738595405, -2.1960708525729915, 1.307122944501224,
                1.7428831194881398, 2.6350367278911766, 4.342522743070935,
                4.44043983890006, 3.507611158507424, 4.593991347420627,
                3.1924205644196757, 2.320271763996685, 0.7144218976567184,
                0.172947266650471, -2.509039156615088, -2.4357585309247574,
                -3.581403283948949, -3.1644045240635035, -5.261783850002523,
                -5.784799008753093, -3.0307056434362267, -4.047984869050489,
                -0.6937827034641011, 2.623034162901131]

        ,
        "x": [[0.7523793648417161], [2.529169757643311], [0.37723338368658244],
              [4.265220701700198], [3.575687412565057], [5.205946488733838],
              [4.647590921236449], [3.181209487847961], [3.1342973543078596],
              [1.5872499303592211], [1.1632526587026335], [-1.3321531826320436],
              [-2.7876256096983716], [-4.104715473706946], [-5.199326929508319],
              [-5.7104822580345465], [-4.917193464796935], [-4.170180717896176],
              [-2.912434152473489], [-0.7649255740698633],
              [-1.8181844383298196], [0.6138272928173297], [2.8198967655379166],
              [3.657444773611683], [4.065355997081614], [5.340924461579886],
              [4.660801519267923], [3.4917981866106818], [1.719671986267815],
              [-0.9965905962165857], [-1.5159868221053883],
              [0.023053628461934483], [-1.3942704111052848],
              [-3.7762377237404783], [-4.712577914494647], [-5.989601371326241],
              [-5.190648524592266], [-3.0014943363836215], [-4.350029511228147],
              [-1.975201263344441], [-1.2125450967315816],
              [-0.7324303001695451], [3.505381340106536], [2.963476457684033],
              [6.058243701499818], [4.62158040705248], [5.0074845863700785],
              [5.153753700276936], [1.3733079247350108], [2.9452094810896945],
              [-0.5004282319242168], [-0.591585093138894], [-3.269210789754931],
              [-3.5413564503294257], [-3.093031166321828], [-4.547167914731143],
              [-3.9888734399360306], [-4.735873413308104], [-2.312474684121589],
              [-2.493304707377084], [-0.02879983695809149],
              [1.0177348074707853], [2.4349406225018537], [4.553489610681803],
              [5.634423056022638], [4.39066706914744], [3.2643862705101414],
              [4.999179859519946], [4.296450892852596], [2.823775805680633],
              [0.5617802054570316], [-2.238829683663008], [-2.6550645541375952],
              [-3.614580227294506], [-2.8550150314822487], [-5.956771534325955],
              [-6.72976376889799], [-3.5324394386286486], [-1.839721738595405],
              [-2.1960708525729915], [1.307122944501224], [1.7428831194881398],
              [2.6350367278911766], [4.342522743070935], [4.44043983890006],
              [3.507611158507424], [4.593991347420627], [3.1924205644196757],
              [2.320271763996685], [0.7144218976567184], [0.172947266650471],
              [-2.509039156615088], [-2.4357585309247574], [-3.581403283948949],
              [-3.1644045240635035], [-5.261783850002523], [-5.784799008753093],
              [-3.0307056434362267], [-4.047984869050489],
              [-0.6937827034641011], [2.623034162901131]]
    }

    steps = [
        ('pre_scaling', StandardScaler()),
        ('features', FeatureUnion([
            ('ar_transformer', AutoregressiveTransformer(num_lags=3)),
            ('seasonal_transformer', SeasonalTransformer(seasonal_period=20)
             )])),
        ('post_features_imputer', ReversibleImputer()),
        ('regressor', LinearRegression(fit_intercept=False))
    ]

    pipeline = ForecasterPipeline(steps)
    X = np.array(trainReq.get("x"))
    y = np.array(trainReq.get("y"))

    pipeline.fit(X, y)

    model_name = trainReq.get("name")
    pickle.dump(pipeline, open(model_name, 'wb'))

    # predict
    y_pred = pipeline.predict(X, to_scale=True, refit=True)

    plt.plot(y, lw=2)
    plt.plot(y_pred, lw=2)
    # plt.legend(['y_true', 'y_pred'], bbox_to_anchor=(1, 1))

    # forcast
    start_idx = 70
    # plt.plot(y, lw=2)
    y_forcast = pipeline.forecast(y[:, np.newaxis], start_idx=start_idx)
    plt.plot(y_forcast, lw=2)
    ax = plt.gca()
    ylim = ax.get_ylim()
    plt.plot((start_idx, start_idx), ylim, lw=4)
    plt.ylim(ylim)
    plt.legend(['y_true', 'y_pred', 'y_forcast',
                'forecast start'], bbox_to_anchor=(1, 1))

    plt.show()


def test():
    steps = [
        ('pre_scaling', StandardScaler()),
        ('features', FeatureUnion([
            ('ar_transformer', AutoregressiveTransformer(num_lags=3)),
            ('seasonal_transformer', SeasonalTransformer(seasonal_period=20)
             )])),
        ('post_features_imputer', ReversibleImputer()),
        ('regressor', LinearRegression(fit_intercept=False))
    ]

    l = np.linspace(0, 1,
                    101)  # from 0 to 1 inclusive, steps 101;   shape (101,)
    y = 5 * np.sin(2 * np.pi * 5 * l) + np.random.normal(0, 1,
                                                         size=101)  # shape (101,)
    X = y[:, np.newaxis]  # shape (101,1)
    print(y.tolist())
    print(X.tolist())

    pipeline = ForecasterPipeline(steps)

    pipeline.fit(X, y)

    model_name = 'time-series.md'
    pickle.dump(pipeline, open(model_name, 'wb'))

    # predict
    y_pred = pipeline.predict(X, to_scale=True, refit=True)

    plt.plot(y, lw=2)
    plt.plot(y_pred, lw=2)
    # plt.legend(['y_true', 'y_pred'], bbox_to_anchor=(1, 1))

    # forcast
    start_idx = 70
    # plt.plot(y, lw=2)
    y_forcast = pipeline.forecast(y[:, np.newaxis], start_idx=start_idx)
    plt.plot(y_forcast, lw=2)
    ax = plt.gca()
    ylim = ax.get_ylim()
    plt.plot((start_idx, start_idx), ylim, lw=4)
    plt.ylim(ylim)
    plt.legend(['y_true', 'y_pred', 'y_forcast',
                'forecast start'], bbox_to_anchor=(1, 1))

    plt.show()


if __name__ == '__main__':
    test2()
