import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
import pickle

matplotlib.use('TkAgg')

def train_with(x_list, y_list, model_path):
    model = linear_model.LinearRegression()
    X = np.array(x_list)
    y = np.array(y_list)
    model.fit(X, y)
    # model.coef_ -> [2.0], model.intercept_ -> 1.4
    # y = coef_ * x + intercept_
    pickle.dump(model, open(model_path, 'wb'))
    return model_path

    #
    # # y_pred = model.predict(X_test)
    #
    # print("=====y_pred=====")
    # print(y)
    #
    # # The coefficients
    # print('Coefficients: \n', model.coef_)
    # # The mean squared error
    # print('Mean squared error: %.2f'
    #       % mean_squared_error(y_test, y_pred))
    # # The coefficient of determination: 1 is perfect prediction
    # print('Coefficient of determination: %.2f'
    #       % r2_score(y_test, y_pred))
    #
    # # Plot outputs
    # plt.scatter(X_train, y_train,  color='red')
    #
    # plt.scatter(X_test, y_pred,  color='black')
    # # plt.plot(X_test, y_pred, color='blue', linewidth=3)
    #
    # plt.xticks(())
    # plt.yticks(())
    #
    # plt.show()


# def train1():
#
#     # Load the diabetes dataset
#     diabetes_X, diabetes_y = datasets.load_diabetes(return_X_y=True)
#
#     # Use only one feature
#     diabetes_X = diabetes_X[:, np.newaxis, 2]
#
#     # Split the data into training/testing sets
#     diabetes_X_train = diabetes_X[:-20]
#     diabetes_X_test = diabetes_X[-20:]
#
#     # Split the targets into training/testing sets
#     diabetes_y_train = diabetes_y[:-20]
#     diabetes_y_test = diabetes_y[-20:]
#
#     # Create linear regression object
#     regr = linear_model.LinearRegression()
#
#     # Train the model using the training sets
#     regr.fit(diabetes_X_train, diabetes_y_train)
#
#     # Make predictions using the testing set
#     diabetes_y_pred = regr.predict(diabetes_X_test)
#
#     # The coefficients
#     print('Coefficients: \n', regr.coef_)
#     # The mean squared error
#     print('Mean squared error: %.2f'
#           % mean_squared_error(diabetes_y_test, diabetes_y_pred))
#     # The coefficient of determination: 1 is perfect prediction
#     print('Coefficient of determination: %.2f'
#           % r2_score(diabetes_y_test, diabetes_y_pred))
#
#     # Plot outputs
#     plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
#     plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=3)
#
#     plt.xticks(())
#     plt.yticks(())
#
#     plt.show()

def test():
    X = [[1],[2],[3],[4],[5],[6],[7],[8],[9],[10]]
    y = [3.553538342,4.200221546,7.910983783, 8.31686198,12.75722719,14.70310104,16.92054472,17.73183313,19.46103369,20.34839423]
    model_path = "linreg-sample1.md"
    train_with(X, y, model_path=model_path)

if __name__ == '__main__':
    test()