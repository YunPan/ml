import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pickle

matplotlib.use('TkAgg')

def predict_with(x_list, model_path):
    model = pickle.load(open(model_path, 'rb'))

    X = np.array(x_list)
    y_pred = model.predict(X)
    print(f'linreg: {model_path} -> {y_pred}')
    return y_pred

def test():
    X = [[0], [1],[2], [3], [4], [5],[6], [7], [8], [9], [10]]
    model_path = 'linreg-sample1.md'

    y_pred = predict_with(X, model_path)
    print(f"test() -> {y_pred}")

    X_train = np.array([[1],[2],[3],[4],[5],[6],[7],[8],[9],[10]])
    y_train = np.array([3.553538342,
                  4.200221546,
                  7.910983783,
                  8.31686198,
                  12.75722719,
                  14.70310104,
                  16.92054472,
                  17.73183313,
                  19.46103369,
                  20.34839423])

    plt.scatter(X_train, y_train, color='red')
    plt.plot(X, y_pred, color='blue')
    plt.show()

if __name__ == '__main__':
    test()
